import Vue from 'vue'
import VueRouter from 'vue-router'
const Home = () => import('../views/Home.vue')
const Login = () => import('../components/auth/Login')
const Logout = () =>  import('../components/auth/Logout')
const Register = () =>  import('../components/auth/Register')
const Profile = () =>  import('../components/user/Profile')
const Dashboard = () => import('../components/dashboard/Dashboard')
const BuildingCreate = () => import ('../components/building/BuildingCreate')
const BuildingAll = () => import('../components/building/BuildingAll')
const AmenityBuildingCreate = () => import('../components/amenity/AmenityBuildingCreate')
const AmenityBuildingAll = () => import ('../components/amenity/AmenityBuildingAll')
const MainSearch = () => import('../components/public/search/MainSearch')
const ForgotPassword = () => import('../components/public/reset_password/ForgotPassword')
const ResetPassword = () =>  import('../components/public/reset_password/ResetPassword')
/*Admin*/
const UserAll = () => import('../components/admin/user/UserAll')
const CategoryAll = () => import ('../components/admin/category/CategoryAll')
/*Global*/
//import CategoryAllS from '../components/global/select/CategoryAllSelect'
const MultipleUploadFile = () =>  import('../components/global/MultipleUploadFile')
/*Floors*/
const FloorBuildingCreate = () => import('../components/floor/FloorBuildingCreate')
const FloorBuildingAll = () => import('../components/floor/FloorBuildingAll')

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta:{
        requiresVisitor:true,
    }
  },
  {
    path: '/logout',
    name: 'Logout',
    component: Logout,
    meta:{
        requiresAuth:true,
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta:{
        requiresVisitor:true,
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile,
    meta:{
        requiresAuth:true,
    }
  },
  {
    path: '/my-buildings',
    name: 'BuildingAll',
    component: BuildingAll,
    meta:{
        requiresAuth:true,
    }
  },
  {
    path: '/building-create',
    name: 'BuildingCreate',
    component: BuildingCreate,
    meta:{
        requiresAuth:true,
    }
  },
  {
    path: '/building-amenities/:id',
    name: 'AmenityBuildingAll',
    component: AmenityBuildingAll,
    meta:{
        requiresAuth:true,
    }
  },
  {
    path: '/amenity-building-create',
    name: 'AmenityBuildingCreate',
    component: AmenityBuildingCreate,
    meta:{
        requiresAuth:true,
    }
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
    meta:{
        requiresAuth:true,
    }
  },
  {
    path: '/about/:search',
    name: 'About',
      // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    /*component: function () {
   //   return import(/* webpackChunkName: "about" */// '../views/About.vue')*/
   // },*/
    meta:{
        requiresVisitor:true,
    }
  },
  {
    path: '/search/',
    name: 'MainSearch',
    component: MainSearch,
  },
  /**Admin**/
  {
    path: '/admin/users',
    name: 'UserAll',
    component: UserAll,
    meta:{
        requiresAuth:true,
    }
  },
  {
    path: '/admin/categories',
    name: 'CategoryAll',
    component: CategoryAll,
    meta:{
        requiresAuth:true,
    }
  },
  /**Global**/
  /*{
    path: '/categories-all-select',
    name: 'CategoryAllSelect',
    component: CategoryAllS,
    meta:{
        requiresAuth:true,
    }
  },*/
  {
    path: '/multiple-upload-file',
    name: ' MultipleUploadFile',
    component:  MultipleUploadFile,
    meta:{
        requiresAuth:true,
    }
  },
  /**Password request and reset**/
  {
    path: '/reset-password',
    name: 'reset-password',
    component: ForgotPassword,
    meta: {
        requiresVisitor:true,
    }
  },
  {
    path: '/reset-password/:token',
    name: 'reset-password-form',
    component: ResetPassword,
    meta: {
        requiresVisitor:true,
    }
  },
  /**Floors**/
  {
    path: '/floor-building-create',
    name: 'FloorBuildingCreate',
    component: FloorBuildingCreate,
    meta:{
        requiresAuth:true,
    }
  },
  {
      path: '/floors-building/:id',
      name: 'FloorBuildingAll',
      component: FloorBuildingAll,
      meta:{
          requiresAuth:true,
      }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: routes,
  scrollBehavior (to, from, savedPosition) {
      if (savedPosition) {
          return savedPosition
      } else {
          return { x: 0, y: 0 }
      }
  }
})

export default router
