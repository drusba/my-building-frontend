import Vue from 'vue'
import Main from './components/layouts/Main'
import router from './router'
import {store} from './store'
import axios from "axios";

axios.interceptors.response.use(function (response) {
    return response
}, function (error) {
    switch (error.response.status) {
        case 403:
            document.location.href = "/";
            break;
        case 401:
            localStorage.removeItem('access_token');
            localStorage.removeItem('user');
            document.location.href = "/";
            break;
    }
    return Promise.reject(error)
})
/*js*/
import { BootstrapVue } from 'bootstrap-vue'
import VueSweetalert2 from 'vue-sweetalert2';
/**/
/*css*/
import 'bootstrap/dist/css/bootstrap.min.css'
import 'sweetalert2/dist/sweetalert2.min.css'
import 'vue2-timepicker/dist/VueTimepicker.css'
import '@/assets/css/building.scss'

/**/
window.eventBus = new Vue()
Vue.use(BootstrapVue)
Vue.use(VueSweetalert2)
Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!store.getters.loggedIn) {
            next({
                name: 'Login'
            })
        } else {
            next()
        }
    } else if (to.matched.some(record => record.meta.requiresVisitor)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (store.getters.loggedIn) {
            next({
                name: 'Dashboard'
            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
})



new Vue({
  router,
  store,
  render: function (h) { return h(Main) }
}).$mount('#app')
