import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex)
Vue.use(VueAxios, axios)

if(window.location.hostname === 'localhost'){
    axios.defaults.baseURL = 'http://mybuild.bo/api'
}else{
    axios.defaults.baseURL = 'https://backend.mybuilding.odyssey-solution.com/api'
}

export const store = new Vuex.Store({
    state: {
        token: localStorage.getItem('access_token')|| null,
        user: localStorage.getItem('user')|| null,
    },
    getters:{
        loggedIn(state){
            return state.token != null
        },
        userAuth(state){
            return state.user
        },
        baseUrlApi(){
            if(window.location.hostname === 'localhost'){
                return 'http://mybuild.bo/'
            }else{
                return 'https://backend.mybuilding.odyssey-solution.com/'
            }
        }
    },
    mutations: {
        retrieveToken(state,token){
            state.token = token
        },
        getUser(state,data){
            state.user = data
        },
        destroyToken(state){
            state.token = null
        },
        destroyUser(state){
            state.user = null
        },
        destroyRoles(state){
            state.roles = null
        },
    },
    actions: {
        /**Auth**/
        /*Get token for authentication*/
        retrieveToken(context, credentials) {
            return new Promise((resolve,reject) =>{
                axios.post('/login', {
                    username: credentials.username,
                    password: credentials.password,
                }).then((response) => {
                    const token = response.data.access_token;
                    localStorage.setItem('access_token', token);
                    context.commit('retrieveToken', token);
                    context.commit('destroyUser');
                    context.dispatch('getUser');
                    resolve(response)
                }).catch(error => {
                    reject(error)
                });
            });
        },
        /*Destroy Token */
        destroyToken(context){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.post('/logout')
                        .then((response) => {
                            localStorage.removeItem('access_token');
                            localStorage.removeItem('user');
                            context.commit('destroyToken');
                            resolve(response)
                        }).catch(error => {
                        localStorage.removeItem('access_token');
                        localStorage.removeItem('user');
                        context.commit('destroyToken');
                        reject(error)
                    });
                });
            }
        },
        /*Register new user*/
        register(context,data){
            return new Promise((resolve,reject) =>{
                axios.post('/register',{
                    name: data.name,
                    email: data.email,
                    password: data.password,
                    password_confirmation: data.password_confirmation,
                })
                    .then((response) => {
                        resolve(response)
                    }).catch(error => {
                        reject(error)
                });
            });
        },
        /*Get User information*/
        getUser(context){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.get('/user')
                        .then((response) => {
                            const user = JSON.stringify(response.data.user);
                            localStorage.setItem('user', user);
                            context.commit('getUser', user);
                            resolve(response)
                        }).catch(error => {
                        reject(error)
                    });
                });
            }
        },
        /*Update account user*/
        updateProfile(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            return new Promise((resolve,reject) =>{
                axios.put('/user/update/'+ data.id ,{
                    name: data.name,
                })
                    .then((response) => {
                        resolve(response)
                    }).catch(error => {
                        reject(error)
                });
            });
        },
        /**End Auth**/
        /**Buildings**/
        /*Create building*/
        createBuilding(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn) {
                return new Promise((resolve, reject) => {
                    axios.post('/new_building', {
                        name: data.name,
                        address: data.address,
                        description: data.description,
                        categories: data.categories,
                        images: data.images,
                    })
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                            reject(error)
                    });
                });
            }
        },
        /*Get all user buildings */
        getBuildings(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.get('/buildings?page='+data.page)
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                        reject(error)
                    });
                });
            }
        },
        updateBuilding(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.put('/update_building/'+ data.id, {
                        name: data.name,
                        address: data.address,
                        description: data.description,
                        images: data.images,
                        categories: data.categories,
                    }).then((response) => {
                        resolve(response)
                    }).catch(error => {
                        reject(error)
                    });
                });
            }
        },
        /*Get all buildings */
        getPublicBuildings(context,data){
            return new Promise((resolve,reject) =>{
                axios.get('/public-buildings')
                    .then((response) => {
                        resolve(response)
                    }).catch(error => {
                    reject(error)
                });
            });
        },
        /*Search buildings */
        searchBuildings(context,data){
            return new Promise((resolve,reject) =>{
                axios.post(data.url,{
                    filters: data.filters,
                    page: data.page
                })
                    .then((response) => {
                        resolve(response)
                    }).catch(error => {
                    reject(error)
                });
            });
        },
        /**End Buildings**/
        /**Amenities**/
        /*Create Amenity*/
        createAmenity(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn) {
                return new Promise((resolve, reject) => {
                    axios.post('/new_amenity', {
                        building_id: data.building_id,
                        name: data.name,
                        code: data.code,
                        open_at: data.open_at,
                        close_at: data.close_at,
                        description: data.description,
                    })
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                            reject(error)
                    });
                });
            }
        },
        /*Get all user buildings */
        getAmenityBuilding(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.get('/amenities?building_id='+data.building_id+'&page='+data.page)
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                        reject(error)
                    });
                });
            }
        },
        updateAmenityBuilding(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.put('/update_amenity/'+ data.id, {
                        building_id: data.building_id,
                        name: data.name,
                        code: data.code,
                        open_at: data.open_at,
                        close_at: data.close_at,
                        description: data.description,
                    }).then((response) => {
                        resolve(response)
                    }).catch(error => {
                        reject(error)
                    });
                });
            }
        },
        /**End Amenities**/
        /**Delete Method most of all models**/
        remove(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.delete(data.url+data.id)
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                        reject(error)
                    });
                });
            }
        },
        /**Upload File**/
        uploadFile(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve, reject) => {
                    axios.post('/upload_file', {
                        file: data.file,
                        type: data.type,
                        model_name: data.model_name,
                        image_name:data.image_name,
                        id:data.id,
                    })
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                            reject(error)
                    });
                });
            }
        },
        /**Categories**/
        getAllCategories(context){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.get('/all_categories')
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                        reject(error)
                    });
                });
            }
        },
        getPublicAllCategories(context){
            return new Promise((resolve,reject) =>{
                axios.get('/all_public_categories')
                    .then((response) => {
                        resolve(response)
                    }).catch(error => {
                    reject(error)
                });
            });
        },
        /**Admin requests**/
        getAllUsers(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.get('/admin/users?page='+data.page)
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                        reject(error)
                    });
                });
            }
        },
        getUserRoles(context){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.get('/admin/roles')
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                        reject(error)
                    });
                });
            }
        },
        updateUserRole(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.post('/admin/update_role', {
                        id: data.id,
                        role: data.role
                    })
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                        reject(error)
                    });
                });
            }
        },
        updateStatusUser(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.post('/admin/update_user_status', {
                        id: data.id,
                        check: data.check
                    }).then((response) => {
                        resolve(response)
                    }).catch(error => {
                        reject(error)
                    });
                });
            }
        },

        createCategory(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve, reject) => {
                    axios.post('/category-create', {
                        name: data.name,
                        description: data.description,
                        avatar:  data.avatar,
                        image_name: data.image_name
                    })
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                            reject(error)
                    });
                });
            }
        },
        updateCategory(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.put('/category-update/'+ data.id, {
                        name: data.name,
                        description: data.description
                    })
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                        reject(error)
                    });
                });
            }
        },
        getCategories(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.get('/categories?page='+data.page)
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                            reject(error)
                    });
                });
            }
        },
        /**Floors**/
        createFloors(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve, reject) => {
                    axios.post('/new_floors', {
                        floors_number: data.floors_number,
                        building_id: data.building_id,
                    })
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                        reject(error)
                    });
                });
            }
        },
        getFloors(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.get('/building_floors?page='+data.page+'&building_id='+data.building_id)
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                        reject(error)
                    });
                });
            }
        },
        updateFloor(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.put('/update_floor/'+ data.id, {
                        floor_number: data.floor_number,
                    }).then((response) => {
                        resolve(response)
                    }).catch(error => {
                        reject(error)
                    });
                });
            }
        },
        /**Apartments**/
        createApartment(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve, reject) => {
                    axios.post('/new_apartment', {
                        apartment_number: data.apartment_number,
                        type_of_apartment: data.type_of_apartment,
                        floor_id: data.floor_id,
                        building_id: data.building_id,
                    })
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                        reject(error)
                    });
                });
            }
        },
        getApartments(context,data){
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
            if(context.getters.loggedIn){
                return new Promise((resolve,reject) =>{
                    axios.get('/building_apartments?page='+data.page+'&building_id='+data.building_id+'&floor_id='+data.floor_id)
                        .then((response) => {
                            resolve(response)
                        }).catch(error => {
                        reject(error)
                    });
                });
            }
        },
        /**Password request and reset**/
        requestResetPassword(context,data){
            return new Promise((resolve, reject) => {
                axios.post('/password_email', {
                    email: data.email,
                })
                    .then((response) => {
                        resolve(response)
                    }).catch(error => {
                    reject(error)
                });
            });
        },
        resetPassword(context,data){
            return new Promise((resolve,reject) =>{
                axios.post('/password_reset',{
                    token: data.token,
                    email: data.email,
                    password: data.password,
                    password_confirmation: data.password_confirmation

                })
                    .then((response) => {
                        resolve(response)
                    }).catch(error => {
                    reject(error)
                });
            });
        },
    },
    modules: {
    }
})
